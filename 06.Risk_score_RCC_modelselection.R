#### Karine Alcala
#### 20 july 2021
#### Kidney function description -> Part II Risk models
## By flexible models

#Update April 2023: new UKB dataset

rm(list=ls())
setwd("/data/IET_Projects/work/KarineAlcala/Kidney_Function/")

library(tidyverse)
library(lubridate)
library(survival)
library(cowplot)
library(rstpm2)
library(pROC)
library(RColorBrewer)

##Loading data---------

UKB_original<-read.csv("Files/UKB_RCC_final_imputed_April2023_v1.csv")
dim(UKB_original) #463178    101

#only the risk analysis variables, I removed them here to avoid issue if using the wrong variable
UKB<-UKB_original %>% select(-c(Time_Recr_Diag_firstcancer,Time_Recr_Endfup_firstcancer,RCC_firstcancer))

## preparation of data----
UKB<-UKB %>% mutate(BMI_cat=case_when(BMI_imp<25~"<25",
                                      BMI_imp>=25 & BMI_imp<30~"[25-30[",
                                      BMI_imp>=30 & BMI_imp<35~"[30-35[",
                                      BMI_imp>=35~">=35"))
UKB$BMI_cat<-factor(UKB$BMI_cat,levels=c("<25","[25-30[","[30-35[",">=35"),ordered = T)

UKB<-UKB %>% mutate(Age_cat=case_when(Age_Recr<=60~"<=60",
                                      Age_Recr>60 & Age_Recr<=70~"]60-70]",
                                      Age_Recr>70~">70"))
UKB$Age_cat<-factor(UKB$Age_cat,levels=c("<=60","]60-70]",">70"),ordered = T)

UKB<-UKB %>% mutate(RCC_firstRCC=case_when(RCC_firstRCC=="Yes"~1,
                                           RCC_firstRCC=="No"~2,
                                           TRUE~NA_real_))
UKB$RCC_firstRCC<-as.factor(UKB$RCC_firstRCC)
#Rename
UKB<-UKB %>% rename(RCC=RCC_firstRCC, Time_Recr_Diag=Time_Recr_Diag_firstRCC,Time_Recr_Endfup=Time_Recr_Endfup_firstRCC)


#Create RCC at x years, from 1 to 10y
#And time to event
for(i in 1:10){
  UKB<-eval(parse(text=paste0("UKB %>% mutate(RCC_",i,"y=case_when(Time_Recr_Endfup>",i,"~0,
                                        Time_Recr_Endfup<=",i," & RCC==1~1,
                                        Time_Recr_Endfup<=",i," & RCC==0~0,
                                        TRUE~NA_real_))")))
  
  UKB<-eval(parse(text=paste0("UKB %>% mutate(Time_event_",i,"y=case_when(Time_Recr_Endfup>=",i,"~",i,",
                                               Time_Recr_Endfup<",i,"~Time_Recr_Endfup))")))
}

table(UKB$RCC_2y,useNA = "ifany") #150
table(UKB$RCC_5y,useNA = "ifany") #472
table(UKB$RCC,useNA = "ifany") #1447

#Use log but not sd, with a patient with can have his value of eGFR but we will not be able to estimate the sd.
UKB$eGFR_creat_log<-log(UKB$eGFR_creat)
UKB$eGFR_cystC_log<-log(UKB$eGFR_cystC)
UKB$eGFR_creat_cystC_log<-log(UKB$eGFR_creat_cystC)
UKB$CystatinC_log<-log(UKB$Cystatin.C)
UKB$Creatinine_log<-log(UKB$Creatinine)
UKB$CRP_log<-log(UKB$C.reactive.protein)

UKB$eGFR_creat_log_sd<-(log(UKB$eGFR_creat)-mean(log(UKB$eGFR_creat)))/sd(log(UKB$eGFR_creat))
UKB$eGFR_cystC_log_sd<-(log(UKB$eGFR_cystC)-mean(log(UKB$eGFR_cystC)))/sd(log(UKB$eGFR_cystC))
UKB$eGFR_creat_cystC_log_sd<-(log(UKB$eGFR_creat_cystC)-mean(log(UKB$eGFR_creat_cystC)))/sd(log(UKB$eGFR_creat_cystC))
UKB$CystatinC_log_sd<-(log(UKB$Cystatin.C)-mean(log(UKB$Cystatin.C)))/sd(log(UKB$Cystatin.C))
UKB$Creatinine_log_sd<-(log(UKB$Creatinine)-mean(log(UKB$Creatinine)))/sd(log(UKB$Creatinine))
UKB$CRP_log_sd<-(log(UKB$C.reactive.protein)-mean(log(UKB$C.reactive.protein)))/sd(log(UKB$C.reactive.protein))

## Calculate Risk score based on David model----
#Source: https://pubmed.ncbi.nlm.nih.gov/33335022/

#Five years of prediction:
#Recalculate intercept:
#original intercept: -14.6

source("Scripts/RCC_Risk_score_function.R")

#Run David Risk score on all UKB
for(i in 1:10){
  eval(parse(text=paste0("UKB$Risk_score_original_",i,"y<-mapply(risk_score_RCC,UKB$Age_Recr,UKB$Sex,UKB$BMI_imp,UKB$Smoking_status_imp,UKB$Hypertension_imp,UKB$SystolicBP_imp,UKB$DiastolicBP_imp,",i,")")))
}
#Run Pred model (only modifiable risk factors on all UKB)
UKB$Pred_RCC<-mapply(pred_score_RCC,UKB$Age_Recr,UKB$Sex,UKB$BMI_imp,UKB$Smoking_status_imp,UKB$Hypertension_imp,UKB$SystolicBP_imp,UKB$DiastolicBP_imp)

#Keep only people with a risk score
summary(UKB$Risk_score_original_5y) #0 NA
summary(UKB$Risk_score_original_10y) #0 NA
summary(UKB$Risk_score_original_2y) #0 NA

##Egfr_cyst distribution----
plot(density(UKB$eGFR_cystC,na.rm=T),ylim=c(0,0.045))
lines(density(UKB$eGFR_creat,na.rm=T),col="red")
lines(density(UKB$eGFR_creat_cystC,na.rm=T),col="blue")
plot(density(UKB$Creatinine,na.rm=T))
lines(density(UKB$Cystatin.C,na.rm=T),col="red")
#normal

qqnorm(UKB$eGFR_creat_cystC)
qqline(UKB$eGFR_creat_cystC)

#Plot risk in function of egfr----
ggplot(UKB,aes(x=Age_Recr,y=Risk_score_original_5y,color=eGFR_cystC_cat))+geom_smooth()

##test linearity of the risk score---

ggplot(UKB,aes(x=Time_Recr_Endfup,y=Risk_score_original_10y))+geom_smooth()

UKB$Risk_score_original_10y_sd<-UKB$Risk_score_original_10y/sd(UKB$Risk_score_original_10y)
flex_test<-stpm2(Surv(Time_Recr_Endfup,as.numeric(RCC))~Risk_score_original_10y_sd+Age_Recr+Sex,data=UKB,df=2,tvc=list(Risk_score_original_10y_sd=3))
png("Results/Risk_models/HR_Risk_score_original_UKB.png")
plot(flex_test,newdata=data.frame(Risk_score_original_10y_sd=mean(UKB$Risk_score_original_10y_sd),Age_Recr=mean(UKB$Age_Recr),Sex=2),var='Risk_score_original_10y_sd',type='hr',xlim=c(12,0),main="HR for fitted Risk score")
dev.off()

##Split dataset into training(60%) and testing(40%)-------
set.seed(123)
idfortraining_cases<-sample(x=UKB$ID,size=round(nrow(UKB)*0.6,0),replace = F)
idfortraining_ctrl<-sample(x=UKB$ID,size=round(nrow(UKB)*0.6,0),replace = F)
idfortraining<-c(idfortraining_cases,idfortraining_ctrl)
UKB_training<-UKB[UKB$ID %in% idfortraining,]
UKB_testing<-UKB[!(UKB$ID %in% idfortraining),]
nrow(UKB_training)+nrow(UKB_testing)==nrow(UKB) #T

#Save it:
write_tsv(UKB_testing,"Files/UKB_testing_set_v6.tsv")
write_tsv(UKB_training,"Files/UKB_training_set_v6.tsv")

# ##Try different Flexible model----
datarecord<-as.data.frame(matrix(ncol=6, nrow=11))
colnames(datarecord)<-c("Model","Intercept","Interaction","Splines","CRP","AIC")
i<-1
UKB_training$RCC<-as.numeric(as.character(UKB_training$RCC))

#Risk score alone
flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))))
datarecord[i,]<-c("Risk score alone","Yes","NA","NA","NA",round(AIC(flex1),0))
datarecord[i+1,]<-c("Risk score alone","No","NA","NA","NA",round(AIC(flex1_noint),0))
i<-i+2

#splines/log/CRP
flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log+CRP_log_sd, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=1))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log+CRP_log_sd-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=1))
datarecord[i,]<-c("eGFR_creat_cystC_log","Yes","No","1","Yes",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC_log","No","No","1","Yes",round(AIC(flex1_noint),0))
i<-i+2

flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log+CRP_log_sd, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=2))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log+CRP_log_sd-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=2))
datarecord[i,]<-c("eGFR_creat_cystC_log","Yes","No","2","Yes",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC_log","No","No","2","Yes",round(AIC(flex1_noint),0))
i<-i+2

flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log+CRP_log_sd, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=3))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log+CRP_log_sd-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=3))
datarecord[i,]<-c("eGFR_creat_cystC_log","Yes","No","3","Yes",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC_log","No","No","3","Yes",round(AIC(flex1_noint),0))
i<-i+2

#splines/log/no CRP
flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=1))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=1))
datarecord[i,]<-c("eGFR_creat_cystC_log","Yes","No","1","No",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC_log","No","No","1","No",round(AIC(flex1_noint),0))
i<-i+2

flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=2))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=2))
datarecord[i,]<-c("eGFR_creat_cystC_log","Yes","No","2","No",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC_log","No","No","2","No",round(AIC(flex1_noint),0))
i<-i+2

flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=3))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC_log=3))
datarecord[i,]<-c("eGFR_creat_cystC_log","Yes","No","3","No",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC_log","No","No","3","No",round(AIC(flex1_noint),0))
i<-i+2

#splines/not-log/CRP
flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC+CRP_log_sd, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=1))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC+CRP_log_sd-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=1))
datarecord[i,]<-c("eGFR_creat_cystC","Yes","No","1","Yes",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC","No","No","1","Yes",round(AIC(flex1_noint),0))
i<-i+2

flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC+CRP_log_sd, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=2))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC+CRP_log_sd-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=2))
datarecord[i,]<-c("eGFR_creat_cystC","Yes","No","2","Yes",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC","No","No","2","Yes",round(AIC(flex1_noint),0))
i<-i+2

flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log+CRP_log_sd, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=3))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC+CRP_log_sd-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=3))
datarecord[i,]<-c("eGFR_creat_cystC","Yes","No","3","Yes",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC","No","No","3","Yes",round(AIC(flex1_noint),0))
i<-i+2

#splines/not-log/noCRP
flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=1))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=1))
datarecord[i,]<-c("eGFR_creat_cystC","Yes","No","1","No",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC","No","No","1","No",round(AIC(flex1_noint),0))
i<-i+2

flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=2))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=2))
datarecord[i,]<-c("eGFR_creat_cystC","Yes","No","2","No",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC","No","No","2","No",round(AIC(flex1_noint),0))
i<-i+2

flex1<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC_log, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=3))
flex1_noint<-stpm2(Surv(time = Time_Recr_Endfup, event=RCC)~Pred_RCC+eGFR_creat_cystC-1, data=UKB_training,smooth.formula=~nsx(log(Time_Recr_Endfup),Boundary.knots = c(log(1),log(9))), tvc=list(eGFR_creat_cystC=3))
datarecord[i,]<-c("eGFR_creat_cystC","Yes","No","3","No",round(AIC(flex1),0))
datarecord[i+1,]<-c("eGFR_creat_cystC","No","No","3","No",round(AIC(flex1_noint),0))
i<-i+2

datarecord %>% arrange(as.numeric(AIC))
#Lowest: Intercept, No Interaction + 1 splines+notlog+CRP

write_tsv(datarecord,"Results/Risk_models/Flexiblemodel_parameters_AIC.tsv")
